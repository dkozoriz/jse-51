package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public ProjectRemoveByIdCommand() {
        super("project-remove-by-id", "remove project by id.");
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        getEndpointLocator().getProjectEndpoint().projectRemoveById(new ProjectRemoveByIdRequest(getToken(), id));
    }

}