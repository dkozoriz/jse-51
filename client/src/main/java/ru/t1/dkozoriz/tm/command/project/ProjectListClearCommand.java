package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectListClearRequest;

public final class ProjectListClearCommand extends AbstractProjectCommand {

    public ProjectListClearCommand() {
        super("project-clear", "delete all projects.");
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getEndpointLocator().getProjectEndpoint().projectListClear(new ProjectListClearRequest(getToken()));
    }

}