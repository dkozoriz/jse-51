package ru.t1.dkozoriz.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_session")
public class Session extends UserOwnedModel {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    private Date date = new Date();

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
