package ru.t1.dkozoriz.tm.dto.response.user;

import lombok.Getter;
import lombok.Setter;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;

@Getter
@Setter
public class RemoveUserResponse extends AbstractResponse {

}