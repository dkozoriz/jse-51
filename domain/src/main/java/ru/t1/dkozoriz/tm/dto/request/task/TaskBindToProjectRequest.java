package ru.t1.dkozoriz.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    String projectId;

    @Nullable
    String taskId;

    public TaskBindToProjectRequest(
            @Nullable final String token,
            @Nullable String projectId,
            @Nullable String taskId
    ) {
        super(token);
        this.projectId = projectId;
        this.taskId = taskId;
    }

}