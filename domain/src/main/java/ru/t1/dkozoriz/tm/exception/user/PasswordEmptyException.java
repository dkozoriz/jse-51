package ru.t1.dkozoriz.tm.exception.user;

import ru.t1.dkozoriz.tm.exception.field.AbstractFieldException;

public final class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty.");
    }

}