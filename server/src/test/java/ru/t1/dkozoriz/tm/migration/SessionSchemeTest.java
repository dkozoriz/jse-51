package ru.t1.dkozoriz.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class SessionSchemeTest extends AbstractSchemeTest {

    @Test
    public void sessionTest() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("session");
    }

}